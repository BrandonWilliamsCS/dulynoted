/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package marioandlugio.dulynoted.view;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import marioandlugio.common.jfxplus.view.View;

/**
 *
 * @author Brandon Williams
 */
public class FlashCardView extends View {
    
    private StackPane promptContainer;
    private StackPane answerContainer;
    private StackPane controlsContainer;
    private Button nextButton;
    
    @Override
    protected Parent buildViewComponent() {
        VBox column = new VBox();
        ObservableList<Node> children = column.getChildren();
        
        // prompt area
        promptContainer = new StackPane();
        // TODO: extract view code into class or at least function
        promptContainer.setAlignment(Pos.CENTER);
        promptContainer.setPrefSize(400, 300);
        promptContainer.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        children.add(promptContainer);
        
        // response area
        answerContainer = new StackPane();
        answerContainer.setAlignment(Pos.CENTER);
        answerContainer.setPrefSize(400, 300);
        answerContainer.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        children.add(answerContainer);
        
        // controls
        controlsContainer = new StackPane();
        controlsContainer.setAlignment(Pos.CENTER_RIGHT);
        controlsContainer.setPadding(new Insets(5, 5, 5, 5));
        nextButton = new Button("Next");
        controlsContainer.getChildren().add(nextButton);
        children.add(controlsContainer);
        
        return column;
    }
    
    public void setOnNextClick(Runnable onNextClick) {
        this.nextButton.setOnAction((e) -> onNextClick.run());
    }
    
    public void setPrompt(String promptType) {
        ObservableList<Node> children = promptContainer.getChildren();
        children.clear();
        children.add(new Label(promptType));
    }
    
    public void setAnswer(String answerType) {
        ObservableList<Node> children = answerContainer.getChildren();
        children.clear();
        children.add(new Label(answerType));
    }
    
}
