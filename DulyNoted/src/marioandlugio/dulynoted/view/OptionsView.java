/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.view;

import javafx.beans.property.BooleanProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import marioandlugio.common.jfxplus.view.View;

/**
 *
 * @author Brandon Williams
 */
public class OptionsView extends View {

    private Button startButton;

    public void setOnStartClick(Runnable onStartClick) {
        this.startButton.setOnAction((e) -> onStartClick.run());
    }

    //<editor-fold desc="View Component Build">
    @Override
    public Parent buildViewComponent() {
        BorderPane fullPane = new BorderPane();
        fullPane.setBottom(getActionPanel());
        fullPane.setCenter(getChoicesPanel());
        return fullPane;
    }

    private Node getActionPanel() {
        startButton = new Button("Start");
        HBox container = new HBox();
        container.setAlignment(Pos.BOTTOM_RIGHT);
        container.getChildren().add(startButton);
        return container;
    }

    private Node getChoicesPanel() {
        Parent promptPanel = getIOTypePanel("Prompt Types");
        ObservableList<Node> panelChildren = promptPanel.getChildrenUnmodifiable();
        registerCheckbox("promptKeys", (CheckBox) panelChildren.get(1));
        registerCheckbox("promptStaff", (CheckBox) panelChildren.get(2));
        registerCheckbox("promptLetter", (CheckBox) panelChildren.get(3));

        Parent answerPanel = getIOTypePanel("Answer Types");
        panelChildren = answerPanel.getChildrenUnmodifiable();
        registerCheckbox("answerKeys", (CheckBox) panelChildren.get(1));
        registerCheckbox("answerStaff", (CheckBox) panelChildren.get(2));
        registerCheckbox("answerLetter", (CheckBox) panelChildren.get(3));

        FlowPane container = new FlowPane();
        ObservableList children = container.getChildren();
        children.add(promptPanel);
        children.add(answerPanel);
        return container;
    }

    private Parent getIOTypePanel(String titleText) {
        Label title = new Label(titleText);
        CheckBox keyOption = new CheckBox("Piano Keys");
        CheckBox staffOption = new CheckBox("Staff");
        CheckBox notationOption = new CheckBox("Letter Notation");

        VBox container = new VBox();
        ObservableList children = container.getChildren();
        children.add(title);
        children.add(keyOption);
        children.add(staffOption);
        children.add(notationOption);
        container.setPadding(new Insets(10));
        return container;
    }
    //</editor-fold>

//    private Node getSpecialOptionsPanel() {
//        
//    }
}
