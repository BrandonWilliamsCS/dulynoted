/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.model.Music;

/**
 *
 * @author Brandon Williams
 */
public class NotePosition {

    private final int offset; // absolute, from bottom line of staff

    private NotePosition(int offset) {
        this.offset = offset;
    }

    public boolean isSpace() {
        return offset % 2 == 0;
    }

    public boolean isLine() {
        return offset % 2 != 0;
    }

    public boolean isAboveStaff() {
        return offset > 8;
    }

    public boolean isBelowStaff() {
        return offset < 0;
    }

    public int getLineNumber() {
        if (isSpace()) {
            throw new IllegalStateException("Cannot get the line number for a space.");
        }
        int lineOffset = offset / 2;
        return offset >= 0 ? lineOffset + 1 : lineOffset;
    }

    public int getSpaceNumber() {
        if (isLine()) {
            throw new IllegalStateException("Cannot get the space number for a line.");
        }
        int spaceOffset = offset > 0 ? (offset + 1) / 2 : (offset - 1) / 2;
        return spaceOffset;
    }

    public int getDiatonicDifference(NotePosition other) {
        return this.offset - other.offset;
    }

    public NotePosition moveDiatonicSteps(int moveSize) {
        return new NotePosition(this.offset + moveSize);
    }

    // <editor-fold desc="Factory Methods">
    public static NotePosition getLine(int lineNumber) {
        int offset = (lineNumber - 1) * 2;
        return new NotePosition(offset);
    }

    public static NotePosition getLedgerLine(int ledgerLineNumber, boolean aboveStaff) {
        int offsetFromStaff = ledgerLineNumber * 2;
        if (aboveStaff) {
            return new NotePosition(offsetFromStaff + 8);
        } else {
            return new NotePosition(-1 * ledgerLineNumber);
        }
    }

    public static NotePosition getSpace(int spaceNumber) {
        int offset = 2 * spaceNumber - 1;
        return new NotePosition(offset);
    }

    public static NotePosition getLedgerSpace(int ledgerSpaceNumber, boolean aboveStaff) {
        int offsetFromStaff = 2 * ledgerSpaceNumber - 1;
        if (aboveStaff) {
            return new NotePosition(offsetFromStaff + 8);
        } else {
            return new NotePosition(-1 * offsetFromStaff);
        }
    }
    // </editor-fold>
}
