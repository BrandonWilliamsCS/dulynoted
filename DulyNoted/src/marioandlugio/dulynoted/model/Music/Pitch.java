/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.model.Music;

import java.util.Objects;

/**
 *
 * @author Brandon Williams
 */
public class Pitch {

    private final PitchClass pitchClass;
    private final int octave;

    public Pitch(PitchClass pitchClass, int octave) {
        this.pitchClass = pitchClass;
        this.octave = octave;
    }

    public PitchClass getPitchClass() {
        return pitchClass;
    }

    public int getOctave() {
        return octave;
    }

    /**
     * Moves in semitones up from this pitch, as if on a piano with arbitrarily
     * many keys
     *
     * @param semitoneCount
     * @return the pitch that is semitoneCount half-steps above this one
     */
    public Pitch sharpen(int semitoneCount) {
        // thinking of Pitch as a two-digit number, with the octave representing the tens digit and the not representing the ones.
        // Treat the incoming semitone count as a note with as high of an octave as possible.
        int pitchTopSummand = this.pitchClass.semitoneDifference(PitchClass.C);
        int pitchBottomSummand = semitoneCount % 12;
        int octaveTopSummand = this.octave;
        int octaveBottomSummand = semitoneCount / 12;

        int pitchResult = (pitchTopSummand + pitchBottomSummand) / 12;
        int octaveCarry = (pitchTopSummand + pitchBottomSummand) / 12;
        int octaveResult = octaveTopSummand + octaveBottomSummand + octaveCarry;
        PitchClass pitchClassResult = PitchClass.C.sharpen(pitchResult);
        return new Pitch(pitchClassResult, octaveResult);
    }

    public Pitch sharpen() {
        return sharpen(1);
    }

    public Pitch flatten() {
        return sharpen(-1);
    }

    /**
     * Equivalent to "this - other"
     *
     * @param other the pitch to be "subtracted" from this one
     * @return the number of semitones above other this is
     */
    public int semitoneDifference(Pitch other) {
        return (this.octave - other.octave) * 12 + this.pitchClass.semitoneDifference(other.pitchClass);
    }

    /**
     * Moves in "white notes" up from this pitch, as if on a piano with
     * arbitrarily many keys
     *
     * @param noteCount
     * @return the pitch class that is noteCount notes above this one
     */
    public Pitch diatonicSharpen(int noteCount) {
        // thinking of Pitch as a two-digit number, with the octave representing the tens digit and the not representing the ones.
        // Treat the incoming semitone count as a note with as high of an octave as possible.
        int pitchTopSummand = this.pitchClass.diatonicDifference(PitchClass.C);
        int pitchBottomSummand = noteCount % 7;
        int octaveTopSummand = this.octave;
        int octaveBottomSummand = noteCount / 7;

        int pitchResult = (pitchTopSummand + pitchBottomSummand) / 7;
        int octaveCarry = (pitchTopSummand + pitchBottomSummand) / 7;
        int octaveResult = octaveTopSummand + octaveBottomSummand + octaveCarry;
        PitchClass pitchClassResult = PitchClass.C.diatonicSharpen(pitchResult);
        return new Pitch(pitchClassResult, octaveResult);
    }

    public Pitch diatonicSharpen() {
        return diatonicSharpen(1);
    }

    public Pitch diatonicFlatten() {
        return diatonicSharpen(-1);
    }

    /**
     * Equivalent to "this - other"
     *
     * @param other the pitch to be "subtracted" from this one
     * @return the number of notes above other this is
     */
    public int diatonicDifference(Pitch other) {
        return (this.octave - other.octave) * 7 + this.pitchClass.diatonicDifference(other.pitchClass);
    }
    
    public Pitch increaseOcvate(int octaveCount) {
        return new Pitch(pitchClass, octave + octaveCount);
    }

    public Pitch increaseOcvate() {
        return increaseOcvate(1);
    }

    public Pitch decreaseOcvate() {
        return increaseOcvate(-1);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.pitchClass);
        hash = 23 * hash + this.octave;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Pitch other = (Pitch) obj;
        return this.octave == other.octave && Objects.equals(this.pitchClass, other.pitchClass);
    }
}
