/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.model.Music;

import java.util.Objects;

/**
 *
 * @author Brandon Williams
 */
public class Note {

    private final Pitch pitch;
    private final NoteValue noteValue;
    private final int adjustmentDots;

    public Note(Pitch pitch, NoteValue noteValue, int adjustmentDots) {
        this.pitch = pitch;
        this.noteValue = noteValue;
        this.adjustmentDots = adjustmentDots;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public boolean isRest() {
        return pitch == null;
    }

    public NoteValue getNoteValue() {
        return noteValue;
    }

    public int getAdjustmentDots() {
        return adjustmentDots;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.pitch);
        hash = 71 * hash + Objects.hashCode(this.noteValue);
        hash = 71 * hash + this.adjustmentDots;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Note other = (Note) obj;
        return Objects.equals(this.pitch, other.pitch)
                && this.noteValue == other.noteValue
                && this.adjustmentDots == other.adjustmentDots;
    }
}
