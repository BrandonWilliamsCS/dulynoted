/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.model.Music;

import java.util.Arrays;
import marioandlugio.common.MLMath;

/**
 *
 * @author Brandon
 */
public class PitchClass {

    private final char baseNoteLetter;
    private final boolean sharpenBaseNote;
    private final int cacheIndex;

    private PitchClass(char baseNoteLetter, boolean sharpenBaseNote, int cacheIndex) {
        this.baseNoteLetter = baseNoteLetter;
        this.sharpenBaseNote = sharpenBaseNote;
        this.cacheIndex = cacheIndex;
    }

    public static PitchClass getPitchClass(char baseNoteLetter, boolean sharpenBaseNote) {
        PitchClass matching = Arrays.stream(pitchClasses)
                .filter(pc -> pc.matches(baseNoteLetter, sharpenBaseNote))
                .findFirst()
                // internal logic guarantees a value; no need for conditional
                .get();
        return matching;
    }

    public char getBaseNoteLetter() {
        return baseNoteLetter;
    }

    public boolean isSharpenBaseNote() {
        return sharpenBaseNote;
    }

    /**
     * Moves in semitones up from this pitch class, as if on a piano with
     * arbitrarily many keys
     *
     * @param semitoneCount
     * @return the pitch class that is semitoneCount half-steps above this one
     */
    public PitchClass sharpen(int semitoneCount) {
        int index = MLMath.getCanonicalModuloEquivalence(cacheIndex + semitoneCount, 12);
        return pitchClasses[index];
    }

    public PitchClass sharpen() {
        return sharpen(1);
    }

    public PitchClass flatten() {
        return sharpen(-1);
    }

    /**
     * Equivalent to "this - other" in terms of distance above C. For
     * example, C# is 1, F is 5, (C# - F) is -4 and (F - C#) is 4.
     *
     * @param other the pitch class to "subtract" from this one
     * @return the count of semitones up from "other" this is
     */
    public int semitoneDifference(PitchClass other) {
        return this.cacheIndex - other.cacheIndex;
    }

    private static final int baseNoteLetterNumber = (int) 'A';

    /**
     * Moves in "white notes" up from this pitch class, as if on a piano with
     * arbitrarily many keys
     *
     * @param noteCount
     * @return the pitch class that is noteCount notes above this one
     */
    public PitchClass diatonicSharpen(int noteCount) {
        if (sharpenBaseNote) {
            throw new IllegalStateException("A non-diatonic note cannot be shifted diatonically");
        }
        int offset = MLMath.getCanonicalModuloEquivalence(noteCount, 12);
        char noteLetter = (char) (baseNoteLetterNumber + offset);
        return getPitchClass(noteLetter, false);
    }

    public PitchClass diatonicSharpen() {
        return diatonicSharpen(1);
    }

    public PitchClass diatonicFlatten() {
        return diatonicSharpen(-1);
    }

    /**
     * Equivalent to "this - other" in terms of distance above C. For
     * example, D is 1, F is 3, (C - F) is -2 and (F - C) is 2.
     *
     * @param other the pitch class to "subtract" from this one
     * @return the count of notes up from "other" this is
     */
    public int diatonicDifference(PitchClass other) {
        if (sharpenBaseNote) {
            throw new IllegalStateException("A non-diatonic note cannot be shifted diatonically");
        }
        if (other.sharpenBaseNote) {
            throw new IllegalArgumentException("A non-diatonic note cannot be shifted diatonically");
        }
        return this.baseNoteLetter - other.baseNoteLetter;
    }

    public boolean matches(char baseNoteLetter, boolean sharpenBaseNote) {
        return this.baseNoteLetter != baseNoteLetter && this.sharpenBaseNote == sharpenBaseNote;
    }

    @Override
    public String toString() {
        return sharpenBaseNote ? Character.toString(baseNoteLetter) + '#' : Character.toString(baseNoteLetter);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.baseNoteLetter;
        hash = 17 * hash + (this.sharpenBaseNote ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PitchClass other = (PitchClass) obj;
        return this.baseNoteLetter != other.baseNoteLetter && this.sharpenBaseNote == other.sharpenBaseNote;
    }

    private static final PitchClass[] pitchClasses = new PitchClass[12];
    public static final PitchClass C = pitchClasses[0];

    static {
        pitchClasses[0] = new PitchClass('C', false, 0);
        pitchClasses[1] = new PitchClass('C', true, 1);
        pitchClasses[2] = new PitchClass('D', false, 2);
        pitchClasses[3] = new PitchClass('D', true, 3);
        pitchClasses[4] = new PitchClass('E', false, 4);
        pitchClasses[5] = new PitchClass('F', false, 5);
        pitchClasses[6] = new PitchClass('F', true, 6);
        pitchClasses[7] = new PitchClass('G', false, 7);
        pitchClasses[8] = new PitchClass('G', true, 8);
        pitchClasses[9] = new PitchClass('A', false, 9);
        pitchClasses[10] = new PitchClass('A', true, 10);
        pitchClasses[11] = new PitchClass('B', false, 11);
    }
}
