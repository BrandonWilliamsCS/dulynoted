/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.model.Music;

/**
 *
 * @author Brandon Williams
 */
public class Staff {

    // eventually, we could add a time signature feature as well.
    private final Pitch clefAnchorPitch;
    private final NotePosition clefPosition;

    // For the sake of DulyNoted, don't worry about using non-treble clefs.
    public Staff() {
        // default to treble clef: a g clef on the second line.
        PitchClass clefAnchorPitchClass = PitchClass.getPitchClass('G', false);
        clefAnchorPitch = new Pitch(clefAnchorPitchClass, 4);
        clefPosition = NotePosition.getLine(2);
    }

    public Pitch getPitchAt(NotePosition position) {
        int diatonicDifference = clefPosition.getDiatonicDifference(position);
        return clefAnchorPitch.diatonicSharpen(diatonicDifference);
    }

    public NotePosition getPositionFor(Pitch pitch) {
        int diatonicDifference = clefAnchorPitch.diatonicDifference(pitch);
        return clefPosition.moveDiatonicSteps(diatonicDifference);
    }
}
