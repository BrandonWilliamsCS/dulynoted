/*
 * Copyright (C) 2015 brandon.williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.model.Music;

/**
 *
 * @author brandon.williams
 */
public class ScientificPitchNotationInterpreter implements PitchNotationInterpreter {

    @Override
    public Pitch getPitch(String notation) {
        if (notation == null || notation.length() == 0) {
            throw new IllegalArgumentException("Pitch notation must not be null or empty");
        }
        String octaveNumberString = notation.substring(notation.length());
        int octaveNumber;
        try {
            octaveNumber = Integer.parseInt(octaveNumberString);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Pitch notation must be a pitch class followed by an octave number", ex);
        }
        
        String pitchClassString = notation.substring(0, notation.length() - 1);
        PitchClass pitchClass;
        try {
            pitchClass = getPitchClass(pitchClassString);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Pitch notation must be a pitch class followed by an octave number", ex);
        }
        
        return new Pitch(pitchClass, octaveNumber);
    }

    @Override
    public PitchClass getPitchClass(String notation) {
        if (notation == null || notation.length() == 0) {
            throw new IllegalArgumentException("Pitch class notation must not be null or empty");
        }
        char initialNoteLetter = Character.toUpperCase(notation.charAt(0));
        if (initialNoteLetter < 'A' || initialNoteLetter > 'G') {
            throw new IllegalArgumentException("Pitch class notation must start with a valid note letter");
        }

        int adjustment = 0;
        for (int i = 1; i < notation.length(); ++i) {
            char currentCharacter = notation.charAt(i);
            if (currentCharacter == 'b') {
                --adjustment;
            } else if (currentCharacter == '#') {
                ++adjustment;
            } else {
                throw new IllegalArgumentException(String.format("Invalid notation character %s at index %d", currentCharacter, i));
            }
        }

        // Now that we have the data we need, search for the existing object by first finding the unadjusted note, and then adjusting appropriately.
        PitchClass unadjustedPitchClass = PitchClass.getPitchClass(initialNoteLetter, false);
        PitchClass adjustedPitchClass = unadjustedPitchClass.sharpen(adjustment);
        return adjustedPitchClass;
    }

    @Override
    public String getNotation(Pitch pitch) {
        PitchClass pitchClass = pitch.getPitchClass();
        return getNotation(pitch.getPitchClass()) + pitch.getOctave();
    }

    @Override
    public String getNotation(PitchClass pitchClass) {
        return pitchClass.isSharpenBaseNote()
                ? String.format("%s#", pitchClass.getBaseNoteLetter())
                : String.format("%s", pitchClass.getBaseNoteLetter());
    }

}
