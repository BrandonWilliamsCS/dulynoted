/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted.controller;

import marioandlugio.common.jfxplus.controller.Controller;
import marioandlugio.dulynoted.view.OptionsView;
import marioandlugio.common.jfxplus.view.View;

/**
 *
 * @author Brandon Williams
 */
public class OptionsController extends Controller {

    @Override
    public View buildView() {
        OptionsView optionsView = new OptionsView();
        optionsView.ensureViewComponentBuilt();
        optionsView.setOnStartClick(() -> {
            if (!(optionsView.getBooleanValue("promptKeys") || optionsView.getBooleanValue("promptStaff") || optionsView.getBooleanValue("promptLetter"))
                    || !(optionsView.getBooleanValue("answerKeys") || optionsView.getBooleanValue("answerStaff") || optionsView.getBooleanValue("answerLetter")))
                return;
            
            System.out.println("Prompt Keys: " + optionsView.getBooleanValue("promptKeys"));
            System.out.println("Prompt Staff: " + optionsView.getBooleanValue("promptStaff"));
            System.out.println("Prompt Notation: " + optionsView.getBooleanValue("promptLetter"));
            System.out.println("Answer Keys: " + optionsView.getBooleanValue("answerKeys"));
            System.out.println("Answer Staff: " + optionsView.getBooleanValue("answerStaff"));
            System.out.println("Answer Notation: " + optionsView.getBooleanValue("answerLetter"));

            {
                FlashCardController flashCardController = new FlashCardController();
                flashCardController.setValues(
                        optionsView.getBooleanValue("promptKeys"),
                        optionsView.getBooleanValue("promptStaff"),
                        optionsView.getBooleanValue("promptLetter"),
                        optionsView.getBooleanValue("answerKeys"),
                        optionsView.getBooleanValue("answerStaff"),
                        optionsView.getBooleanValue("answerLetter")
                );
                getNagivationHandler().navigateToChild(flashCardController);
                flashCardController.showNextCard();
            }
        });
        return optionsView;
    }

}
