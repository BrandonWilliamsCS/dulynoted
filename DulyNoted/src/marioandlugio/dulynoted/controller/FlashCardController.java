/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package marioandlugio.dulynoted.controller;

import java.util.ArrayList;
import marioandlugio.common.jfxplus.controller.Controller;
import marioandlugio.common.jfxplus.view.View;
import marioandlugio.dulynoted.view.FlashCardView;

/**
 *
 * @author Brandon Williams
 */
public class FlashCardController extends Controller {
    
    private FlashCardView flashCardView;
    private boolean[] storedValues;

    @Override
    protected View buildView() {
        flashCardView = new FlashCardView();
        flashCardView.ensureViewComponentBuilt();
        flashCardView.setOnNextClick(() -> { showNextCard(); });
        return flashCardView;
    }
    
    // TODO: put values in a data structure in model
    public void setValues(boolean ... values) {
        storedValues = values;
    }
    
    public void showNextCard() {
        // get note to present
        
        // determine prompt format
        // TODO: move to options model, which will have much better random selection logic (including ensuring the two are different)
        ArrayList<String> availablePrompts = new ArrayList<>();
        if (storedValues[0])
            availablePrompts.add("Keys");
        if (storedValues[1])
            availablePrompts.add("Staff");
        if (storedValues[2])
            availablePrompts.add("Letter");
        String promptType = availablePrompts.get((int)(Math.random() * availablePrompts.size()));
        
        // determine answer format
        ArrayList<String> availableAnswers = new ArrayList<>();
        if (storedValues[3])
            availableAnswers.add("Keys");
        if (storedValues[4])
            availableAnswers.add("Staff");
        if (storedValues[5])
            availableAnswers.add("Letter");
        String answerType = availableAnswers.get((int)(Math.random() * availableAnswers.size()));
        
        // display each
        flashCardView.setPrompt(promptType);
        flashCardView.setAnswer(answerType);
    }
}
