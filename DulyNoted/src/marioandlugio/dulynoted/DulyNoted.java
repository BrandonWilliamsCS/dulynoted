/*
 * Copyright (C) 2015 Brandon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package marioandlugio.dulynoted;

import static javafx.application.Application.launch;
import marioandlugio.common.jfxplus.WindowSetupData;
import marioandlugio.common.jfxplus.ApplicationPlus;
import marioandlugio.dulynoted.controller.OptionsController;

/**
 *
 * @author Brandon
 */
public class DulyNoted extends ApplicationPlus {

    @Override
    public void applicationSetup(WindowSetupData applicationStartupData) {
        OptionsController controller = new OptionsController();
        controller.setTitle("Duly Noted");
        applicationStartupData.setRootController(controller);
        applicationStartupData.setHeight(400);
        applicationStartupData.setWidth(400);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
